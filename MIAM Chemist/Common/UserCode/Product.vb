﻿
Namespace LightSwitchApplication

    Public Class Product

        Private Sub ProductName_Validate(results As EntityValidationResultsBuilder)

            If Me.ProductName IsNot Nothing Then
                Dim dupes = (From detail In Me.DataWorkspace.ApplicationData.Products
                            Where detail.ProductName.Equals(Me.ProductName) AndAlso
                            detail.Id <> Me.Id).Execute

                If (dupes.Any) Then
                    results.AddPropertyError(Me.ProductName + " is a duplicate entry.")
                End If

            End If

        End Sub
    End Class

End Namespace
