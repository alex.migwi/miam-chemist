﻿
Namespace LightSwitchApplication

    Public Class SalesOrderDetail
        Dim BasePrice As Double
        Private Sub SubTotal_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            If Me.Price > 0 Then
                result = (Me.Quantity * Me.Price) + (Me.Price * Me.Product.Tax.Amount)
            End If
        End Sub

        Private Sub ProductName_Compute(ByRef result As String)
            ' Set result to the desired field value
            If Me.Product IsNot Nothing Then
                Me.ProductName = Me.Product.ProductName
            End If
        End Sub

        Private Sub Quantity_Validate(results As EntityValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")
            If Me.Quantity > 0 Then
                Dim dw = Me.Application.CreateDataWorkspace.ApplicationData.InventoryHeaders
                Dim inventory = (From s In dw
                                   Where s.Product.Id = Me.Product.Id AndAlso
                                         s.InventoryTotal > 0).Execute

                For Each inventoryqty In inventory
                    If inventoryqty.InventoryTotal < Me.Quantity Then
                        results.AddPropertyError("There is not enough inventory to sell for the current product.")
                    End If
                Next

            End If
        End Sub

        Private Sub Product_Changed()
            If Me.Product IsNot Nothing Then
                Me.Price = Me.Product.Price
                Me.BasePrice = Me.Product.Price * 0.8
            End If
        End Sub

        Private Sub Price_Validate(results As EntityValidationResultsBuilder)
            If Me.BasePrice > Me.Price Then
                results.AddPropertyResult("Price entered is below Set Base Price", ValidationSeverity.Error)
            ElseIf Me.Price < 1 Then
                results.AddPropertyError("Price cannot be zero or less than 1")
            End If
        End Sub

        Private Sub Product_Validate(results As EntityValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")
            If Me.Product IsNot Nothing Then
                Dim dupes = From detail In Me.SalesOrderHeader.SalesOrderDetails
                            Where detail.Product IsNot Nothing AndAlso
                            detail.Product.Id = Me.Product.Id AndAlso
                            detail IsNot Me

                If dupes.Any Then
                    results.AddPropertyError(Me.Product.ProductName + "is a duplicate product")
                End If

                Dim dw = Me.Application.CreateDataWorkspace.ApplicationData.InventoryHeaders
                Dim inventory = (From s In dw
                                   Where s.Product.Id = Me.Product.Id).Execute

                For Each inv In inventory
                    If (inv Is Nothing) Then
                        results.AddPropertyError("The current product is out of stock.")
                    End If
                Next
               

            End If
        End Sub
    End Class

End Namespace
