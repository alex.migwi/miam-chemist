﻿
Namespace LightSwitchApplication

    Public Class InvoiceHeader

        Private Sub AmountDue_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            If Me.InvoiceTotal > Me.AmountPaid Then
                result = Me.InvoiceTotal - Me.AmountPaid
            Else
                result = 0.0
            End If
        End Sub

        Private Sub InvoiceTotal_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            Dim total = 0.0

            For Each d In Me.InvoiceDetails
                total += d.Total
            Next
            result = total
        End Sub

        Private Sub InvoiceDate_Validate(results As EntityValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")
            If Me.InvoiceDate > Date.Today Then
                results.AddPropertyError("Invoice date cannot be in the future.")
            End If
        End Sub

        Private Sub AmountPaid_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            Dim total = 0.0

            For Each ip In Me.InvoicePayments
                total += ip.TotalPaid
            Next
            result = total
        End Sub

        Private Sub InvoiceNumber_Validate(results As EntityValidationResultsBuilder)
            If Me.InvoiceNumber IsNot Nothing Then
                Dim dupes = (From detail In Me.DataWorkspace.ApplicationData.InvoiceHeaders
                            Where detail.InvoiceNumber.Equals(Me.InvoiceNumber) AndAlso
                            detail.Id <> Me.Id).Execute

                If (dupes.Any) Then
                    results.AddPropertyError(Me.InvoiceNumber + " is a duplicate entry.")
                End If

            End If
        End Sub
    End Class

End Namespace
