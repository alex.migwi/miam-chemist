﻿
Namespace LightSwitchApplication

    Public Class DailyAccount

        Private Sub CashAtHand_Compute(ByRef result As Decimal)

            ' Set result to the desired field value
            result = Me.BalanceForward + Me.DailySales - Me.DailyExpediture

        End Sub

        Private Sub DailyBalance_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            result = Me.CashAtHand - Me.CashBanked
        End Sub

        Private Sub DailyAccount_Created()
            'Me.BalanceForward = 0.0
            'Me.DailySales = 0.0
            'Me.DailyExpediture = 0.0
            ''Me.CashAtHand = 0.0
            'Me.ChequeBanked = 0.0
            'Me.CashBanked = 0.0
            ''Me.DailyBalance = 0.0
        End Sub
    End Class

End Namespace
