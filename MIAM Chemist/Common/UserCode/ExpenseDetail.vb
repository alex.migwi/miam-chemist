﻿
Namespace LightSwitchApplication

    Public Class ExpenseDetail

        Private Sub ExpenseDetail_Created()
            ' Me.ExpenseName = ""
            Me.Amount = 0.0
        End Sub

        Private Sub ExpenseName_Validate(results As EntityValidationResultsBuilder)
            If Me.ExpenseName IsNot Nothing Then
                Dim dupes = From detail In Me.ExpenseHeader.ExpenseDetails
                            Where detail.ExpenseName IsNot Nothing AndAlso
                            detail.ExpenseName.Equals(Me.ExpenseName) AndAlso
                            detail IsNot Me

                If dupes.Any Then
                    results.AddPropertyError(Me.ExpenseName + " is a duplicate entry, update the quantity of the previous entry to add the new item.")
                End If

            End If
        End Sub
    End Class

End Namespace
