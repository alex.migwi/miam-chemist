﻿
Namespace LightSwitchApplication

    Public Class SalesOrderHeader


        Private Sub OrderTotal_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            Dim total = 0.0

            For Each d In Me.SalesOrderDetails
                total += d.SubTotal
            Next
            result = total
        End Sub

        Private Sub AmountDue_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            result = Me.OrderTotal - Me.AmountPaid
        End Sub

        Private Sub OrderDate_Validate(results As EntityValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")
            If Me.OrderDate > Date.Today Then
                results.AddPropertyError("Order Date cannot be in the future.")
            End If
        End Sub
    End Class

End Namespace
