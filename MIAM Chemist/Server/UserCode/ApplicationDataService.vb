﻿
Namespace LightSwitchApplication

    Public Class ApplicationDataService

        Private Sub Taxes_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteTaxes)
        End Sub

        Private Sub Taxes_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteTaxes)
        End Sub

        Private Sub Taxes_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteTaxes)

        End Sub

        Private Sub Suppliers_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteSuppliers)

        End Sub

        Private Sub Suppliers_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteSuppliers)

        End Sub

        Private Sub Suppliers_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteSuppliers)

        End Sub

        Private Sub SalesOrderHeaders_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteSalesHeader)

        End Sub

        Private Sub SalesOrderHeaders_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteSalesHeader)

        End Sub

        Private Sub SalesOrderHeaders_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteSalesHeader)

        End Sub

        Private Sub SalesOrderDetails_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteSalesDetails)

        End Sub

        Private Sub SalesOrderDetails_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteSalesHeader)

        End Sub

        Private Sub SalesOrderDetails_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteSalesHeader)

        End Sub

        Private Sub Products_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteProducts)

        End Sub

        Private Sub Products_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteProducts)

        End Sub

        Private Sub Products_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteProducts)

        End Sub

        Private Sub InvoiceHeaders_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteInvoiceHeader)

        End Sub

        Private Sub InvoiceHeaders_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteInvoiceHeader)

        End Sub

        Private Sub InvoiceHeaders_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteInvoiceHeader)

        End Sub

        Private Sub InvoiceDetails_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteInvoiceDetails)

        End Sub

        Private Sub InvoiceDetails_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteInvoiceDetails)

        End Sub

        Private Sub InvoiceDetails_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteInvoiceDetails)

        End Sub

        Private Sub InventoryHeaders_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteInventoryHeaders)

        End Sub

        Private Sub InventoryHeaders_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteInventoryHeaders)

        End Sub

        Private Sub InventoryHeaders_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteInventoryHeaders)

        End Sub

        Private Sub ExpenseHeaders_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteExpenseHeaders)

        End Sub

        Private Sub ExpenseHeaders_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteExpenseHeaders)

        End Sub

        Private Sub ExpenseHeaders_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteExpenseHeaders)

        End Sub

        Private Sub ExpenseDetails_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteExpenseDetails)

        End Sub

        Private Sub ExpenseDetails_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteExpenseDetails)

        End Sub

        Private Sub ExpenseDetails_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteExpenseDetails)

        End Sub

        Private Sub Employees_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteEmployee)

        End Sub

        Private Sub Employees_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteEmployee)

        End Sub

        Private Sub Employees_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteEmployee)

        End Sub

        Private Sub DailyAccounts_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteDailyAccounts)

        End Sub

        Private Sub DailyAccounts_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteDailyAccounts)

        End Sub

        Private Sub DailyAccounts_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteDailyAccounts)

        End Sub

        Private Sub Customers_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteCustomer)

        End Sub

        Private Sub Customers_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteCustomer)

        End Sub

        Private Sub Customers_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteCustomer)

        End Sub
        Private Sub StoreDetails_CanDelete(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteStoreDetails)

        End Sub
        Private Sub StoreDetails_CanInsert(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteStoreDetails)

        End Sub
        Private Sub StoreDetails_CanUpdate(ByRef result As Boolean)
            result = Me.Application.User.HasPermission(Permissions.AddEditDeleteStoreDetails)

        End Sub

        Private Sub InvoiceDetails_Inserting(entity As InvoiceDetail)
            Dim dw = Me.DataWorkspace.ApplicationData

            Dim packPieces

            If entity.PackQty > 0 Then
                packPieces = entity.PackQty
            Else
                packPieces = 1
            End If

            Try

                'try to update the product price
                Dim currentProducts = (From cp In dw.Products
                      Where cp.Id = entity.Product.Id).Execute

                For Each currentProduct In currentProducts
                    currentProduct.Price = (1.3 * entity.Price) / packPieces
                    'dw.SaveChanges()
                Next

                'Update the inventory header
                Dim currentInventories = (From ci In dw.InventoryHeaders
                      Where ci.Product.Id = entity.Product.Id).Execute

                For Each currentInventory In currentInventories

                    currentInventory.InventoryTotal += entity.Quantity * packPieces

                    'dw.SaveChanges()
                    'Update the Inventory details

                    Dim currentInventoryDetails = (From cid In dw.InventoryDetails
                      Where cid.InventoryHeader.Id = currentInventory.Id).Execute

                    For Each currentInventoryDetail In currentInventoryDetails
                        currentInventoryDetail.Supplier = entity.InvoiceHeader.Supplier
                        currentInventoryDetail.UnitPrice = entity.Price
                        currentInventoryDetail.Quantity = entity.Quantity * packPieces
                    Next
                Next
                dw.SaveChanges()
            Catch ex As Exception
                Trace.TraceInformation("An error occured please try again!")
            End Try
        End Sub

       

        Private Sub InvoiceDetails_Updating(entity As InvoiceDetail)
            Dim dw = Me.DataWorkspace.ApplicationData

            Dim packPieces

            If entity.PackQty > 0 Then
                packPieces = entity.PackQty
            Else
                packPieces = 1
            End If

            Try

                'try to update the product price
                Dim currentProducts = (From cp In dw.Products
                      Where cp.Id = entity.Product.Id).Execute

                For Each currentProduct In currentProducts
                    currentProduct.Price = (0.3 * entity.Price) / packPieces
                    'dw.SaveChanges()
                Next

                'Update the inventory header
                Dim currentInventories = (From ci In dw.InventoryHeaders
                      Where ci.Product.Id = entity.Product.Id).Execute

                For Each currentInventory In currentInventories

                    currentInventory.InventoryTotal += entity.Quantity * packPieces

                    'dw.SaveChanges()
                    'Update the Inventory details

                    Dim currentInventoryDetails = (From cid In dw.InventoryDetails
                      Where cid.InventoryHeader.Id = currentInventory.Id).Execute

                    For Each currentInventoryDetail In currentInventoryDetails
                        currentInventoryDetail.Supplier = entity.InvoiceHeader.Supplier
                        currentInventoryDetail.UnitPrice = entity.Price
                        currentInventoryDetail.Quantity = entity.Quantity * packPieces
                    Next
                Next
                dw.SaveChanges()
            Catch ex As Exception
                Trace.TraceInformation("An error occured please try again!")
            End Try
        End Sub

        Private Sub Employees_Inserting(entity As Employee)

            Try

                Dim reg = From regs In Me.DataWorkspace.SecurityData.UserRegistrations
                       Where regs.UserName.Equals(entity.UserName)

                If (reg IsNot Nothing) Then
                    'MsgBox("Adding", MsgBoxStyle.Information)

                    Dim newUser = Me.DataWorkspace.SecurityData.UserRegistrations.AddNew()
                    newUser.UserName = entity.UserName
                    newUser.FullName = entity.EmployeeName
                    newUser.Password = "changeme/123"
                    Me.DataWorkspace.SecurityData.SaveChanges()
                End If



            Catch ex As Exception
                Trace.TraceInformation("Fatal Error. An error occured while trying to save related data.")
            End Try

        End Sub

        Private Sub Products_Inserting(entity As Product)

            Try
                Dim ih = Me.DataWorkspace.ApplicationData.InventoryHeaders.AddNew()
                ih.Product = entity
                ih.ReOrderLevel = 0
                ih.InventoryTotal = 0
                'DataWorkspace.ApplicationData.SaveChanges()
                'Me.SaveChanges()

            Catch ex As Exception
                Throw New ArgumentException(ex.ToString)
                Trace.TraceInformation("Fatal Error. An error occured while trying to save related data.")
            End Try
        End Sub

        Private Sub SalesOrderHeaders_Inserted(entity As SalesOrderHeader)
            Dim dw = DataWorkspace.ApplicationData

            Dim newSalePay As OrderPayment = dw.OrderPayments.AddNew()
            With newSalePay
                .PaymentDate = entity.OrderDate
                .SalesOrderHeader = entity
                .Amount = entity.AmountPaid
            End With
        End Sub

        Private Sub OrderPayments_Inserting(entity As OrderPayment)

            'Dim dw = DataWorkspace.ApplicationData
            'Dim count = (From recs In dw.OrderPayments
            '             Select recs).Execute

            'Dim total = 0

            'For Each co In count
            '    total += 1
            'Next

            If entity.SalesOrderHeader.AmountPaid <> entity.Amount Then
                entity.SalesOrderHeader.AmountPaid += entity.Amount
            End If

        End Sub

        Private Sub OrderPayments_Updating(entity As OrderPayment)
            'Insert code here
            'get old val
            Dim oldAmt = entity.Details.Properties.Amount.OriginalValue
            Dim newAmt = entity.Details.Properties.Amount.Value

            entity.SalesOrderHeader.AmountPaid -= oldAmt
            entity.SalesOrderHeader.AmountPaid += newAmt


        End Sub

        Private Sub OrderPayments_Deleting(entity As OrderPayment)

            Dim oldAmt = entity.Details.Properties.Amount.OriginalValue
            Try
                entity.SalesOrderHeader.AmountPaid -= oldAmt

            Catch ex As Exception
                Trace.TraceInformation("An error occured please try again!")
            End Try

        End Sub

        Private Sub BulkPay_PreprocessQuery(FilterTerm As String, ByRef query As System.Linq.IQueryable(Of LightSwitchApplication.InvoiceHeader))

            query = FilterControl.Filter(query, FilterTerm, Me.Application)

        End Sub

        Private Sub SaleOrderBulkPay_PreprocessQuery(FilterTerm As String, ByRef query As System.Linq.IQueryable(Of LightSwitchApplication.SalesOrderHeader))

            query = FilterControl.Filter(query, FilterTerm, Me.Application)

        End Sub

    End Class

End Namespace
