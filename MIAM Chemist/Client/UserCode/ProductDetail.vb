﻿
Namespace LightSwitchApplication

    Public Class ProductDetail

        Private Sub Product_Loaded(succeeded As Boolean)
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.Product)
        End Sub

        Private Sub Product_Changed()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.Product)
        End Sub

        Private Sub ProductDetail_Saved()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.Product)
        End Sub

    End Class

End Namespace