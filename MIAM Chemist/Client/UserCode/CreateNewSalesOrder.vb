﻿
Namespace LightSwitchApplication

    Public Class CreateNewSalesOrder

        Private Sub CreateNewSalesOrder_InitializeDataWorkspace(saveChangesTo As List(Of Microsoft.LightSwitch.IDataService))
            ' Write your code here.
            Me.SalesOrderHeaderProperty = New SalesOrderHeader()

            'Dim storeDet = Me.DataWorkspace.ApplicationData.StoreDetails.FirstOrDefault()

            'Try
            '    If Not storeDet Is Nothing Then
            '        Me.StoreDetails.StoreName = storeDet.StoreName.ToString
            '    End If
            'Catch ex As Exception
            '    Me.ShowMessageBox("Create Store Details using the Administration Menu", "Fatal Error", MessageBoxOption.Ok)
            'End Try

        End Sub

        Private Sub CreateNewSalesOrder_Saved()
            ' Write your code here.
            'Me.Close(False)
            'Application.Current.ShowDefaultScreen(Me.SalesOrderHeaderProperty)
            Me.SalesOrderHeaderProperty = New SalesOrderHeader()
            Me.AmountTendered = 0.0
            Me.ChangeDue = 0.0

        End Sub

        Private Sub AmountTendered_Changed()
            If Me.AmountTendered > Me.SalesOrderHeaderProperty.OrderTotal Then
                Me.SalesOrderHeaderProperty.AmountPaid = Me.SalesOrderHeaderProperty.OrderTotal
                Me.ChangeDue = Me.AmountTendered - Me.SalesOrderHeaderProperty.OrderTotal
            Else
                Me.SalesOrderHeaderProperty.AmountPaid = Me.AmountTendered
            End If
        End Sub

        Private Sub CreateNewSalesOrder_Saving(ByRef handled As Boolean)
            Dim dw = Me.Application.CreateDataWorkspace.ApplicationData
            For Each saledetail In Me.SalesOrderDetails
                Dim inventory = (From s In dw.InventoryHeaders
                               Where s.Product.Id = saledetail.Product.Id).Execute

                For Each inventoryqty In inventory
                    inventoryqty.InventoryTotal -= saledetail.Quantity
                    dw.SaveChanges()
                Next
            Next

        End Sub

        Private Sub CreateNewSalesOrder_Created()
            ' Write your code here.
            Dim myUser As Microsoft.LightSwitch.Security.IUser = Me.Application.User
            'Me.ShowMessageBox(myUser.FullName, "Yep", MessageBoxOption.Ok)
            If (myUser IsNot Nothing) Then
                If (Not myUser.FullName.Equals("Test User")) AndAlso (Not myUser.FullName.Equals("Adminstrator")) Then
                    Me.SalesOrderHeaderProperty.Employee = Me.DataWorkspace.ApplicationData.Employees.Where(
                        Function(status) status.EmployeeName = myUser.FullName).FirstOrDefault()
                    Me.StoreDetails.StoreName = Me.SalesOrderHeaderProperty.Employee.StoreDetail.StoreName
                End If

                Me.SalesOrderHeaderProperty.Customer = Me.DataWorkspace.ApplicationData.Customers.Where(
                        Function(status) status.CustomerName = "Customer").FirstOrDefault()
            End If
            Me.AmountTendered = 0.0
            Me.ChangeDue = 0.0
        End Sub

        Private Sub ViewProductPrices_Execute()
            ' Write your code here.
            Me.Application.ShowSearchProductPrice()
        End Sub
    End Class

End Namespace