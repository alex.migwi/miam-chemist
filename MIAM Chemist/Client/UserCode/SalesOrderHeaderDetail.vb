﻿
Namespace LightSwitchApplication

    Public Class SalesOrderHeaderDetail

        Private Sub SalesOrderHeader_Loaded(succeeded As Boolean)
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.SalesOrderHeader)
        End Sub

        Private Sub SalesOrderHeader_Changed()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.SalesOrderHeader)
        End Sub

        Private Sub SalesOrderHeaderDetail_Saved()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.SalesOrderHeader)
        End Sub

    End Class

End Namespace