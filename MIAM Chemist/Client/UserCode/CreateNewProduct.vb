﻿
Namespace LightSwitchApplication

    Public Class CreateNewProduct

        Private Sub CreateNewProduct_InitializeDataWorkspace(ByVal saveChangesTo As Global.System.Collections.Generic.List(Of Global.Microsoft.LightSwitch.IDataService))
            ' Write your code here.
            Me.ProductProperty = New InventoryDetails()
        End Sub

        Private Sub CreateNewProduct_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.ProductProperty)
        End Sub

    End Class

End Namespace