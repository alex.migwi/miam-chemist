﻿
Namespace LightSwitchApplication

    Public Class Application

        Private Sub AllSales_CanRun(ByRef result As Boolean)
            ' Set result to the desired field value
            result = Me.User.HasPermission(Permissions.AccessAllSalesScreen)
        End Sub

        Private Sub CreateNewSalesOrder_CanRun(ByRef result As Boolean)
            ' Set result to the desired field value
            result = Me.User.HasPermission(Permissions.AccessCreateNewSaleScreen)

        End Sub

        Private Sub DailyAccounts_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessDailyAccountsScreen)

        End Sub

        Private Sub EmployeesListDetail_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccesEmployeeListDetailScreen)

        End Sub

        Private Sub InventoryHeadersListDetail_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessInventoryListDetailScreen)

        End Sub

        Private Sub AddExpeditureDetails_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessExpenseListDetailScreen)

        End Sub

        Private Sub ExpenseReport_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessExpenseReportScreen)

        End Sub

        Private Sub InventoryReport_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessInventoryReportScreen)

        End Sub

        Private Sub InvoiceDetails_CanRun(ByRef result As Boolean, InvoiceHeaderId As Integer)
            result = Me.User.HasPermission(Permissions.AccessInvoiceDetailsScreen)

        End Sub

        Private Sub InvoiceHeaderDetail_CanRun(ByRef result As Boolean, InvoiceHeaderId As Integer)
            result = Me.User.HasPermission(Permissions.AccessInvoiceHeadersListDetailsScreen)

        End Sub

        Private Sub InvoiceHeadersListDetail_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessInvoiceListDetailsScreen)

        End Sub

        Private Sub InvoiceReport_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessInvoiceReportScreen)

        End Sub

        Private Sub ProductsListDetail_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessProductListDetailScreen)

        End Sub

        Private Sub SalesOrderHeaderDetail_CanRun(ByRef result As Boolean, SalesOrderHeaderId As Integer)
            result = Me.User.HasPermission(Permissions.AccessSalesOrderHeaderListScreen)

        End Sub

        Private Sub SuppliersListDetail_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessSuppliesListDetailScreen)

        End Sub

        Private Sub TaxesListDetail_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessTaxesListDetailScreen)

        End Sub

        Private Sub CustomersListDetail_CanRun(ByRef result As Boolean)
            result = Me.User.HasPermission(Permissions.AccessCustomerListDetailScreen)

        End Sub

        Private Sub InvoiceBulkPay_CanRun(ByRef result As Boolean)
            ' Set result to the desired field value
            result = Me.User.HasPermission(Permissions.AddBulkInvoicePay)
        End Sub

        Private Sub SalesOrderBulkPay_CanRun(ByRef result As Boolean)
            ' Set result to the desired field value
            result = Me.User.HasPermission(Permissions.AddBulkSaleOrderPay)
        End Sub
    End Class

End Namespace
