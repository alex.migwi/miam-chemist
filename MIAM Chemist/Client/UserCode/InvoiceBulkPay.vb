﻿
Namespace LightSwitchApplication

    Public Class InvoiceBulkPay

        Private Sub BulkPay_Loaded(succeeded As Boolean)
            Dim ot = 0.0

            'Get All Customers Orders Total
            For Each invoice In BulkPay
                ot = ot + invoice.InvoiceTotal
            Next
            Me.InvoiceTotals = ot

            'Get All Customers Paid Up Orders Total

            Dim ap = 0.0

            For Each invoice In BulkPay
                ap += invoice.AmountPaid
            Next
            Me.TotalPaid = ap

            'Get All Customers Due Orders Total
            Dim ad = 0.0

            For Each invoice In BulkPay
                ad += invoice.AmountDue
            Next
            Me.TotalDue = ad
        End Sub

        Private Sub InvoicePayBulk_Execute()
            ' Write your code here.
            'iterate through all the invoice
            'get cash due in order to insert into payements table
            'set the invoice as fully paid

            Dim dw = Me.DataWorkspace.ApplicationData

            Dim invPay = dw.InvoicePayments

            For Each invoice In BulkPay
                If invoice.AmountPaid <> invoice.InvoiceTotal And invoice.AmountDue > 0 Then
                    'if this is true we insert a new payment
                    Dim invoiceHeda As InvoiceHeader = invoice
                    Dim newBulkPay As InvoicePayment = dw.InvoicePayments.AddNew()
                    With newBulkPay
                        .InvoiceHeader = invoiceHeda
                        .DatePaid = Date.Today
                        .CashAmount = invoice.AmountDue
                    End With
                    dw.SaveChanges()
                ElseIf (Not (invoice.AmountPaid = invoice.InvoiceTotal)) Then
                    Me.ShowMessageBox("Check Invoice " + invoice.InvoiceNumber + ". An error prevents this operation from finishing successfully", "Error", MessageBoxOption.Ok)
                    Exit Sub
                End If
            Next


        End Sub
    End Class

End Namespace
