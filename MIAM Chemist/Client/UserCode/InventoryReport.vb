﻿
Namespace LightSwitchApplication

    Public Class InventoryReport

        Private Sub InventoryHeaders_SelectionChanged()
            Dim csv = 0.0
            Dim ws = Me.Application.CreateDataWorkspace()
            Dim rs = ws.ApplicationData.InventoryHeaders.GetQuery().Execute()
            Dim records = From r In rs

            'Get Cumulative Product Stock Value i.e. csv
            For Each record In records
                csv = csv + record.Product.Price * record.InventoryTotal
            Next

            Me.ProductStockValue = Me.InventoryHeaders.SelectedItem.InventoryDetails.Sum(Function(x) x.UnitPrice) * Me.InventoryHeaders.SelectedItem.InventoryTotal

            'Me.CumulativeStockValue = Me.InventoryHeaders.Sum(Function(x) x.InventoryTotal)

        End Sub
    End Class

End Namespace
