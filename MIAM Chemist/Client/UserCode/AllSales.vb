﻿
Namespace LightSwitchApplication

    Public Class AllSales


        Private Sub SalesOrderHeaders_Changed(e As Collections.Specialized.NotifyCollectionChangedEventArgs)
            Me.TotalSales = Me.SalesOrderHeaders.Sum(Function(x) x.OrderTotal)
            Me.TotalDueSales = Me.SalesOrderHeaders.Sum(Function(x) x.AmountDue)
            Me.TotalPaidSales = Me.SalesOrderHeaders.Sum(Function(x) x.AmountPaid)
        End Sub

        Private Sub StartDate_Validate(results As ScreenValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")
            If StartDate > Date.Today Then
                results.AddPropertyError("You cannot select a date greater than today or in the future")
            End If
        End Sub

        Private Sub EndDate_Validate(results As ScreenValidationResultsBuilder)
            If EndDate > Date.Today Then
                results.AddPropertyError("You cannot select a date greater than today or in the future")
            End If
        End Sub
    End Class

End Namespace
