﻿
Namespace LightSwitchApplication

    Public Class CreateDailyAccount

        Private Sub CreateDailyAccount_InitializeDataWorkspace(ByVal saveChangesTo As Global.System.Collections.Generic.List(Of Global.Microsoft.LightSwitch.IDataService))
            ' Write your code here.
            Me.DailyAccountProperty = New DailyAccount()
        End Sub

        Private Sub CreateDailyAccount_Saved()
            ' Write your code here.
            Me.Close(False)
            Application.Current.ShowDefaultScreen(Me.DailyAccountProperty)
        End Sub

    End Class

End Namespace