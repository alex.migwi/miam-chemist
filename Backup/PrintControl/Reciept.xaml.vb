﻿Imports System.Windows.Printing

Partial Public Class Reciept
    Inherits UserControl
    Private WithEvents pd As PrintDocument
    Public Sub New()
        InitializeComponent()
        pd = New PrintDocument
        RecieptDate.Text = DateTime.Today.ToShortDateString()
    End Sub
    Private Sub PrintButton_Click(ByVal sender As Object, _
            ByVal e As RoutedEventArgs)
        pd.Print(String.Format("Reciept Date: {0}", Date.Today.ToShortDateString()))
    End Sub
    Private Sub pd_PrintPage(ByVal sender As Object, _
            ByVal e As PrintPageEventArgs) Handles pd.PrintPage
        e.PageVisual = LayoutRoot
    End Sub

End Class