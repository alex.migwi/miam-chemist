﻿Namespace LightSwitchApplication

    Public Class InvoiceDetail
        Dim VAT = 0
        Private Sub InvoiceDetail_Created()
            Me.Quantity = 0.0
            Me.Price = 0.0
            Me.Discount = 0.0
            Me.PackQty = 0.0

        End Sub
        Private Sub SubTotal_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            result = Me.Quantity * Me.Price
        End Sub

        Private Sub Total_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            If Me.Product IsNot Nothing Then
                VAT = Me.Product.Tax.Amount
                Dim Vated = (Me.SubTotal + (Me.SubTotal * VAT))
                result = Vated - (Me.Discount * Vated)
            Else
                result = Me.SubTotal - (Me.Discount * Me.SubTotal)
            End If
        End Sub

        Private Sub Product_Validate(results As EntityValidationResultsBuilder)
            If Me.Product IsNot Nothing Then
                Dim dupes = From detail In Me.InvoiceHeader.InvoiceDetails
                            Where detail.Product IsNot Nothing AndAlso
                            detail.Product.Equals(Me.Product) AndAlso
                            detail IsNot Me

                If dupes.Any Then
                    results.AddPropertyError(Me.Product.ProductName + " is a duplicate entry, update the quantity of the previous entry to add the new item.")
                End If

            End If
        End Sub

    End Class

End Namespace
