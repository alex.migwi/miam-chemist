﻿
Namespace LightSwitchApplication

    Public Class InvoicePayment

        Private Sub TotalPaid_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            result = Me.CreditNoteAmount + Me.CashAmount
        End Sub

        Private Sub InvoicePayment_Created()
            Me.CashAmount = 0.0
            Me.CreditNoteAmount = 0.0
        End Sub

        Private Sub DateOnCreditNote_Validate(results As EntityValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")
            If Me.InvoiceHeader IsNot Nothing Then
                Dim MonthEnd = DateSerial(Year(CDate(Me.InvoiceHeader.InvoiceDate)), Month(CDate(Me.InvoiceHeader.InvoiceDate)) + 1, -1)
                Dim MonthStart = DateSerial(Year(CDate(Me.InvoiceHeader.InvoiceDate)), Month(CDate(Me.InvoiceHeader.InvoiceDate)) + 0, +1)

                If Me.DateOnCreditNote < MonthStart Or Me.DateOnCreditNote > MonthEnd Then
                    results.AddPropertyError("Only credit notes within the same month as the invoice can be used.")
                End If
            End If
            

        End Sub

        Private Sub CreditNote_Validate(results As EntityValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")

            If Me.CreditNote IsNot Nothing Then
                Dim dupes = From detail In Me.InvoiceHeader.InvoicePayments
                            Where detail.CreditNote IsNot Nothing AndAlso
                            detail.CreditNote.Equals(Me.CreditNote) AndAlso
                            detail IsNot Me

                If dupes.Any Then
                    results.AddPropertyError(Me.CreditNote + " is a duplicate entry.")
                End If

            End If

        End Sub
    End Class

End Namespace
