﻿
Namespace LightSwitchApplication

    Public Class Employee

        Private Sub UserName_Validate(results As EntityValidationResultsBuilder)
            If Me.UserName IsNot Nothing Then
                Dim dupes = (From detail In Me.DataWorkspace.ApplicationData.Employees
                            Where detail.UserName.Equals(Me.UserName) AndAlso
                            detail.Id <> Me.Id).Execute

                If (dupes.Any) Then
                    results.AddPropertyError(Me.UserName + " is not available.")
                End If

            End If
        End Sub

        Private Sub EmployeeName_Validate(results As EntityValidationResultsBuilder)
            If Me.EmployeeName IsNot Nothing Then
                Dim dupes = (From detail In Me.DataWorkspace.ApplicationData.Employees
                            Where detail.EmployeeName.Equals(Me.EmployeeName) AndAlso
                            detail.Id <> Me.Id).Execute

                If (dupes.Any) Then
                    results.AddPropertyError(Me.EmployeeName + " either already exists or is a duplicate entry.")
                End If

            End If
        End Sub

        Private Sub IdNumber_Validate(results As EntityValidationResultsBuilder)
            If Me.IdNumber IsNot Nothing Then
                Dim dupes = (From detail In Me.DataWorkspace.ApplicationData.Employees
                            Where detail.IdNumber.Equals(Me.IdNumber) AndAlso
                            detail.Id <> Me.Id).Execute

                If (dupes.Any) Then
                    results.AddPropertyError(Me.IdNumber + " already is in use or exists.")
                End If

            End If
        End Sub
    End Class

End Namespace
