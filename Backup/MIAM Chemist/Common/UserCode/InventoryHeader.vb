﻿
Namespace LightSwitchApplication

    Public Class InventoryHeader

        Private Sub InventoryHeader_Created()
            Me.InventoryTotal = 0.0
            Me.ReOrderLevel = 0.0

        End Sub

        Private Sub ProductName_Compute(ByRef result As String)
            ' Set result to the desired field value
            result = Me.Product.ProductName
        End Sub
    End Class

End Namespace
