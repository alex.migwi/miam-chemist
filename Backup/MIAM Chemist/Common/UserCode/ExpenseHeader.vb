﻿
Namespace LightSwitchApplication

    Public Class ExpenseHeader

        Private Sub ExpenseTotal_Compute(ByRef result As Decimal)
            ' Set result to the desired field value
            Dim total = 0.0

            For Each d In Me.ExpenseDetails
                total += d.Amount
            Next
            result = total
        End Sub

        Private Sub ExpeditureDate_Validate(results As EntityValidationResultsBuilder)
            ' results.AddPropertyError("<Error-Message>")
            If ExpeditureDate > Date.Today Then
                results.AddPropertyError("You cannot select a date greater than today or in the future")
            End If
        End Sub

        Private Sub ExpenseHeader_Created()
            Me.ExpeditureDate = Today.Date
            'Me.ExpenseTotal = 0.0
        End Sub
    End Class

End Namespace
