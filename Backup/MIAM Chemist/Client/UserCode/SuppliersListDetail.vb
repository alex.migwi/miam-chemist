﻿
Namespace LightSwitchApplication

    Public Class SuppliersListDetail

        Private Sub Suppliers_SelectionChanged()
            'If (Me.InventoryDetails.SelectedItem IsNot Nothing) Then
            '    Me.Product = Me.InventoryDetails.SelectedItem.InventoryHeader.ProductName
            'End If
            Me.CumulativeInvoiceTotal = Me.Suppliers.SelectedItem.InvoiceHeader.Sum(Function(x) x.InvoiceTotal)

            Dim creditTotal = 0
            Dim supplier = Me.Suppliers.SelectedItem.Id
            Dim creditNotes = (From credits In Me.DataWorkspace.ApplicationData.InvoicePayments
                              Where credits.InvoiceHeader.Id.Equals(supplier)).Execute


            For Each credit In creditNotes
                creditTotal += credit.TotalPaid
            Next

            Me.CumulativeAmountPaid = Me.Suppliers.SelectedItem.InvoiceHeader.Sum(Function(x) x.AmountPaid) + creditTotal


            Me.CumulativeAmountDue = Me.Suppliers.SelectedItem.InvoiceHeader.Sum(Function(x) x.AmountDue)

        End Sub

        Private Sub ImportSuppliers_Execute()
            ' Write your code here.
            OfficeIntegration.Excel.Import(Me.Suppliers)
        End Sub
    End Class

End Namespace
