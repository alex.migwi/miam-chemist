﻿
Namespace LightSwitchApplication

    Public Class SalesOrderBulkPay

        Private Sub SaleOrderBulkPay_Loaded(succeeded As Boolean)

            Dim ot = 0.0

            'Get All Customers Orders Total
            For Each saleOrder In SaleOrderBulkPay
                ot = ot + saleOrder.OrderTotal
            Next
            Me.OrdersTotal = ot

            'Get All Customers Paid Up Orders Total

            Dim ap = 0.0

            For Each saleOrder In SaleOrderBulkPay
                ap += saleOrder.AmountPaid
            Next
            Me.TotalPaid = ap

            'Get All Customers Due Orders Total
            Dim ad = 0.0

            For Each saleOrder In SaleOrderBulkPay
                ad += saleOrder.AmountDue
            Next
            Me.TotalDue = ad

        End Sub

        Private Sub bulkPayOrders_Execute()
            ' Write your code here.
            ' Write your code here.
            'iterate through all the invoice
            'get cash due in order to insert into payements table
            'set the invoice as fully paid

            Dim dw = Me.DataWorkspace.ApplicationData
            Dim ordPay = dw.OrderPayments

            For Each order In SaleOrderBulkPay
                If order.AmountPaid <> order.OrderTotal And order.AmountDue > 0 Then
                    'if this is true we insert a new payment
                    Dim orderHeda As SalesOrderHeader = order
                    Dim newBulkPay As OrderPayment = dw.OrderPayments.AddNew()
                    With newBulkPay
                        .SalesOrderHeader = orderHeda
                        .PaymentDate = Date.Today
                        .Amount = order.AmountDue
                    End With
                ElseIf (Not (order.AmountPaid = order.OrderTotal)) Then
                    Me.ShowMessageBox("Check Order " + order.Id + ". An error prevents this operation from finishing successfully", "Error", MessageBoxOption.Ok)
                    Exit Sub
                End If
            Next
        End Sub
    End Class

End Namespace
