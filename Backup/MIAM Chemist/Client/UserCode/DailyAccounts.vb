﻿
Namespace LightSwitchApplication

    Public Class DailyAccounts

        Private Sub DailyAccounts1_Changed(e As Collections.Specialized.NotifyCollectionChangedEventArgs)
            If e.Action = Collections.Specialized.NotifyCollectionChangedAction.Add Then

                If Me.DailyAccounts1.Count > 1 And e.NewItems.Count = 1 Then
                    Try
                        Dim newAccount As DailyAccount = e.NewItems(0)
                        Dim dw = DataWorkspace.ApplicationData
                        'Dim currentDetail As DailyAccount = Me.DailyAccounts1(e.NewStartingIndex - 1)

                        newAccount.AccountDate = Me.AccountingDate

                        Dim currentDetails = (From cd In dw.DailyAccounts
                              Where cd.AccountDate = Me.AccountingDate.AddDays(-1)).Execute

                        For Each currentDetail In currentDetails
                            If currentDetail.DailyBalance > 0 Then
                                newAccount.BalanceForward = currentDetail.DailyBalance
                            Else
                                newAccount.BalanceForward = 0
                            End If

                        Next
                        'newAccount.BalanceForward = currentDetail.DailyBalance

                        Dim expeditures = (From s In dw.ExpenseHeaders
                              Where s.ExpeditureDate = Me.AccountingDate).Execute

                        For Each expenditure In expeditures
                            newAccount.DailyExpediture += expenditure.ExpenseTotal
                        Next

                        Dim sales = (From t In dw.SalesOrderHeaders
                             Where t.OrderDate = Me.AccountingDate).Execute

                        For Each sale In sales
                            newAccount.DailySales += sale.AmountPaid
                        Next

                    Catch ex As Exception
                        Trace.TraceInformation("An error occured please try again!")
                    End Try
                Else
                    Try
                        Dim newAccount As DailyAccount = e.NewItems(0)

                        newAccount.AccountDate = Me.AccountingDate

                        Dim dw = DataWorkspace.ApplicationData

                        Dim expeditures = (From s In dw.ExpenseHeaders
                              Where s.ExpeditureDate = Me.AccountingDate).Execute

                        For Each expenditure In expeditures
                            newAccount.DailyExpediture += expenditure.ExpenseTotal
                        Next

                        Dim sales = (From t In dw.SalesOrderHeaders
                             Where t.OrderDate = Me.AccountingDate).Execute

                        For Each sale In sales
                            newAccount.DailySales += sale.AmountPaid
                        Next

                    Catch ex As Exception
                        Trace.TraceInformation("An error occured please try again!")
                    End Try
                End If
            End If
        End Sub
    End Class

End Namespace
