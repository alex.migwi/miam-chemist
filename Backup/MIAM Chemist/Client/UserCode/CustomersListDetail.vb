﻿Imports System.Collections.Specialized


Namespace LightSwitchApplication

    Public Class CustomersListDetail

        Private Sub SalesOrderHeaders_SelectionChanged()
            Dim ot = 0.0
            Dim ws = Me.Application.CreateDataWorkspace()
            Dim rs = ws.ApplicationData.SalesOrderHeaders.GetQuery().Execute()
            Dim records = From r In rs

            'Get All Customers Orders Total
            For Each record In records
                ot = ot + record.OrderTotal
            Next
            Me.AllOrdersTotal = ot
                 
            'Get All Customers Paid Up Orders Total

            Dim ap = 0.0

            For Each record In records
                ap += record.AmountPaid
            Next
            Me.TotalAmountPaid = ap

            'Get All Customers Due Orders Total
            Dim ad = 0.0

            For Each record In records
                ad += record.AmountDue
            Next
            Me.TotalAmountDue = ad

        End Sub

        Private Sub ExportCustOrderRecords_Execute()

            Dim myUser As Microsoft.LightSwitch.Security.IUser = Me.Application.User
            Dim stoName = ""
            Dim stoAddr = ""
            Dim stoContact = ""

            If (myUser IsNot Nothing) Then
                'If (Not myUser.FullName.Equals("Test User")) AndAlso (Not myUser.FullName.Equals("Adminstrator")) Then
                Dim ws = Me.Application.CreateDataWorkspace()
                Dim rs = ws.ApplicationData.Employees.GetQuery().Execute()
                Dim records = From r In rs
                              Where myUser.Name = r.UserName
                              Select r.StoreDetail

                'For Each record In records
                '    stoName = record.StoreName
                '    stoAddr = record.Address
                '    stoContact = record.Phone1

                'Next

                Dim stoDets As New List(Of OfficeIntegration.ColumnMapping)
                stoDets.Add(New OfficeIntegration.ColumnMapping("stoName", "stoName"))
                stoDets.Add(New OfficeIntegration.ColumnMapping("stoAddr", "stoAddr"))
                stoDets.Add(New OfficeIntegration.ColumnMapping("stoContact", "stoContact"))

                Dim mappings As New List(Of OfficeIntegration.ColumnMapping)
                mappings.Add(New OfficeIntegration.ColumnMapping("Order Date", "OrderDate"))
                mappings.Add(New OfficeIntegration.ColumnMapping("Order Total", "OrderTotal"))
                mappings.Add(New OfficeIntegration.ColumnMapping("Amount Paid", "AmountPaid"))
                mappings.Add(New OfficeIntegration.ColumnMapping("Amount Due", "AmountDue"))


                Dim summary As New List(Of OfficeIntegration.ColumnMapping)
                summary.Add(New OfficeIntegration.ColumnMapping("All Orders Total", "AllOrdersTotal"))
                summary.Add(New OfficeIntegration.ColumnMapping("Total Amount Paid", "TotalAmountPaid"))
                summary.Add(New OfficeIntegration.ColumnMapping("Tota  l Amount Due", "TotalAmountDue"))


                'OfficeIntegration.Word.Export(records, True, stoDets, False)
                OfficeIntegration.Word.Export(Me.SalesOrderHeaders, True, mappings, True)
                OfficeIntegration.Word.Export(Me.SalesOrderHeaders, True, summary, True)


                'End If
            End If
            ' Write your code here.

        End Sub
    End Class

End Namespace
