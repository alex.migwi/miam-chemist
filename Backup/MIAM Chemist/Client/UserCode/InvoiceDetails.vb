﻿
Namespace LightSwitchApplication

    Public Class InvoiceDetails

        Private Sub InvoiceHeader_Loaded(succeeded As Boolean)
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.InvoiceHeader)
        End Sub

        Private Sub InvoiceHeader_Changed()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.InvoiceHeader)
        End Sub

        Private Sub InvoiceDetails_Saved()
            ' Write your code here.
            Me.SetDisplayNameFromEntity(Me.InvoiceHeader)
        End Sub

    End Class

End Namespace